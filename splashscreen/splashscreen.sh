#!/bin/bash

W=$(cat /sys/class/graphics/fb0/virtual_size | cut -d, -f1) # Get width
H=$(cat /sys/class/graphics/fb0/virtual_size | cut -d, -f2) # Get height

fbset -depth 32

convert -background black -gravity center -extent ${W}x${H} /opt/RPi-tools/splashscreen/splash.png bgra:/dev/fb0 
dd if=/dev/fb0 | md5sum - > /tmp/fb_checksum
sleep 5
dd if=/dev/fb0 | md5sum --quiet -c /tmp/fb_checksum
if [[ $? -eq 0 ]]; then
	dd if=/dev/zero of=/dev/fb0 
else
	echo "Skip flushing framebuffer beceause it have changed since splashscreen"
fi
rm /tmp/fb_checksum

exit 0
